package com.example.ci;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatriceTest {
    private Calculatrice calculatrice;

    @BeforeEach
    void setup(){
        this.calculatrice = new Calculatrice();
    }

    @Test
    @DisplayName("Test d'addition")
    void testAdditionner(){
        assertEquals(15, calculatrice.additionner(10,5), "L'addition de 10 et 5 doit donner 15");
    }

    @Test
    @DisplayName("Test de multiplication")
    void multiplier() {
        assertEquals(20, calculatrice.multiplier(4,5), "La multiplication est fausse");
    }

    @Test
    @DisplayName("La multiplication par zero doit donner zero")
    void multiplierParZero() {
        assertEquals(0, calculatrice.multiplier(0,5), "La multiplication est fausse");
    }

    @Test
    @DisplayName("la division par zero doit echouer")
    void diviserParZero() {
        assertThrows(ArithmeticException.class,() -> calculatrice.diviser(5,0), "La division par zero doit lever une erreur");
    }

    @RepeatedTest(5)
    @DisplayName("La division doit fonctionner plusieurs fois à la suite")
    void diviserTest(){
        assertEquals(5, calculatrice.diviser(10,2));
    }


}