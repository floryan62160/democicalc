package com.example.ci;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.lang.reflect.Executable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AssertTest {

    @Test
    void inegaliteNombre(){
        assertNotEquals(4,5,"4 et 5 ne sont pas egaux");
    }

    @Test
    void inegaliteList(){
        List<String> liste1 = Arrays.asList("a","b","c");
        List<String> liste2 = Arrays.asList("b","a","c");
        assertNotEquals(liste1,liste2);
    }

    @Test
    void testTrue(){
        assertTrue(true);
    }

    @Test
    void egaliteObject(){
        String a = new String("hello");
        String b = new String("hello");
        String c = b;
        assertNotSame(c,b);
    }

    @Test
    void testGroupe(){
        Dimension rect = new Dimension(400,500);
        assertAll("On teste les dimension du rectangle", () ->{
            assertTrue(rect.getHeight() == 500.00, "La hauteur est égale a 500");
        }, () -> {
            assertTrue(rect.getWidth() == 400.00, "La largeur est égale a 400");
        });
    }
}