package com.example.ci;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloTest {
    Hello hello;

    @BeforeEach
    void setup(){
        this.hello = new Hello();
    }

    @Test
    @DisplayName("L'application doit dire bonjour")
    void sayHello(){
        assertEquals("hello", hello.sayHello());
    }

}